<?php

// Prova 1
// Macchina di F1

//Parte Anteriore

abstract class ParteAnteriore {
    abstract public function incidente();
}

class AlaAnteriore extends ParteAnteriore {
    public function incidente(){
        echo "Ala danneggiata \n";
    }
}

class RuoteAnteriori extends ParteAnteriore {
    public function incidente (){
        echo "Foratura \n";
    }
}

class Pilota extends ParteAnteriore {
    public function incidente (){
        echo "Ho sfondato tutto fratelli \n";
    }
}

//Parte Posteriore

abstract class PartePosteriore {
    abstract public function accellera();
}

class Alettone extends PartePosteriore {
    public function accellera(){
        echo "Alettone aperto \n";
    }
    
}

class RuotePosteriori extends PartePosteriore {
    public function accellera(){
        echo "Aumento la mia velocità \n";
    }
}

class Motore extends PartePosteriore {
    public function accellera(){
        echo "Brum brum \n";
    }
}

class F1Car {
    //Richiamo Parti Anteriore
    public $parteAnteriore;
    public $alaAnteriore;
    public $ruoteAnteriori;
    //Richiamo Parti Posteriori
    public $partePosteriore;
    public $ruotePosteriori;
    public $motore;

    public function __construct(ParteAnteriore $ruoteAnteriori,ParteAnteriore $parteAnteriore, ParteAnteriore $alaAnteriore, PartePosteriore $partePosteriore, PartePosteriore $ruotePosteriori, PartePosteriore $motore){
        $this->ruoteAnteriori = $ruoteAnteriori;
        $this->parteAnteriore = $parteAnteriore;
        $this->alaAnteriore = $alaAnteriore;
        $this->partePosteriore = $partePosteriore;
        $this->ruotePosteriori = $ruotePosteriori;
        $this->motore = $motore;
    }
    public function crash(){
        $this->ruoteAnteriori->incidente();
        $this->alaAnteriore->incidente();
        $this->parteAnteriore->incidente();
    }
    public function start(){
        $this->partePosteriore->accellera();
        $this->ruotePosteriori->accellera();
        $this->motore->accellera();
        }
}

$ferrari = new F1Car(new RuoteAnteriori, new Pilota, new AlaAnteriore, new Alettone, new RuotePosteriori, new Motore);

$ferrari->start();
$ferrari->crash();


// Prova 2.

// Computer con :
//1.Hardware (Scheda video, Processore, Alimentatore)
//2.Software (Windows, Discord, VisualStudio)

abstract class Hardware{
    abstract public function energy();
}

class SchedaVideo extends Hardware{
    public function energy(){
        echo "Mando energia nella scheda video \n";
    }
}

class Processore extends Hardware{
    public function energy(){
        echo"Mando energia al processore \n";
    }
}

class Alimentatore extends Hardware{
    public function energy(){
        echo"Mando energia all' alimentatore \n";
    }
}


abstract class Software{
    abstract public function open();
}

class Windows extends Software{
    public function open(){
        echo "Avvio Windows \n";
    }
}

class Discord extends Software{
    public function open(){
        echo "Apro Discord \n";
    }
}

class VisualStudio extends Software{
    public function open(){
        echo "Apro Visual Studio \n";
    }
}

class Pc {
    //avviameto pc, dichiaro variabili hardware
    public $schedaVideo;
    public $processore;
    public $alimentatore;
    //avviamento software, dichiaro variabili software
    public $windows;
    public $discord;
    public $visualStudio;

    public function __construct(Hardware $schedaVideo, Hardware $processore, Hardware $alimentatore, Software $windows, Software $discord, Software $visualStudio){
        $this->schedaVideo = $schedaVideo;
        $this->processore = $processore;
        $this->alimentatore = $alimentatore;

        $this->windows = $windows;
        $this->discord = $discord;
        $this->visualStudio = $visualStudio;
    }

    public function startHardware(){
        $this->schedaVideo->energy();
        $this->processore->energy();
        $this->alimentatore->energy();
    }

    public function openApp(){
        $this->windows->open();
        $this->discord->open();
        $this->visualStudio->open();

    }

}

$computer = new Pc(new SchedaVideo, new Processore, new Alimentatore, new Windows, new Discord, new VisualStudio);

$computer->startHardware();

$computer->openApp();